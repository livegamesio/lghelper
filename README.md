LiveGames Integration
===================

Hello,
This documentation describes of  **MultiWallet** or **SeamlessWallet** Integration with **e-if.js**  iframe library of Livetech N.V games.


*Some expression which used for this document are like below.*
| Expression     | Note                              |
| :----------- | :-------------------------------- |
| **SEAMLESS_URL** | URL of your seamless handler to get connection betwen LG & Provider.                       (Exp:http://www.yoursite.com/livegamesapi ) |
| **ApiKey** | Specific key for each website. |
| **ApiSecret** | to have secure connection between two system its encrypt te connection |
| **JWT** | Encryption method for incoming & outgoing data objects.



----------


User Sign (sign)
-----

To open our games you need to generate JWT token (sign) which include user details. with generated JWT token system checks security & permissions of the requests and accept user data into to the system.

JWT data should include below data for JSON requests;
```json
{
  "apiKey": "YOUR_API_KEY",
  "game": "tombala",
  "iat": 1484864574,
  "exp": 1484950974,
  "jti": "REQUEST_UNIQUE_ID",
  "user": {
    "id": "USERID_123",
    "name": "Display Name(username)",
    "parent": "Reseller"
  }
}
```

| Parameter | Description        |
| :-------- | :-------------------- |
| **game**  | Game that requested|
| **apiKey**  | API_KEY that requested |
| **iat**   | timestamp of TOKEN (IssuedAt) |
| **exp**   | timestamp of TOKEN expiration (Expire) |
| **user**  | User object details which needs include below datas also: |
| | **id :** USER_ID _[MUST-UNIQUE-ALFANUMERIC-32char]_ |
| | **name :** USERNAME(DisplayName) _[OPSIONAL-Username which show in the game UI if you not provide system shows ID - 128char]_ |
| | **parent :** Site-name should provide if not system automatically create parent. |

> <i class="icon-pencil"></i> To find proper JWT library for your infrastructure you can check this site.  http://jwt.io     


##JWT Creation

####**PHP**
This example using  `\Firebase\JWT` library to create JWT.

```php
<?php
// Builds which using Composer this library will be load by autoloader.
if (!class_exists('\Firebase\JWT\JWT')) {
	require_once("lib/jwt/BeforeValidException.php");
	require_once("lib/jwt/ExpiredException.php");
	require_once("lib/jwt/SignatureInvalidException.php");
	require_once("lib/jwt/JWT.php");
}
$apiKey = "YOUR_API_KEY";
$apiSecret = "YOUR_API_SECRET";
$now = time();
$expireIn = 86400;
$userModel = [  //false || userObject
			"id" => "USERID_123",
		    "name" => "Display Name(usernameatGame)",
		    "parent" => "Reseller(Sitename)"
		];

$token = [
				"game" => "tombala",
				"apiKey" => $apiKey,
				"iat" => $now,
				"exp" => $now + $expireIn,
				"jti" => hash('crc32', $apiKey . $now)
				"user" => $userModel,
			];
$jwt = JWT::encode($token, $apiSecret);
```

at below example JWT generated with using  **LiveGames\Client-API-PHP** helper library.

```php
<?php
//Builds which using Composer this library will be load by autoloader.
require_once("../client-api-php/src/Api.php");
//--

$userModel = [
				"id" => "USERID_123",
			    "name" => "Display Name",
			    "parent" => "Reseller"
			];
$api = new \LiveGames\ClientApi\Api($userModel, 'API_KEY','API_SECRET');

//JWT String
echo $api->token;
```



####**Node.js**
https://bitbucket.org/livegamesio/client-api-nodejs

####**.NET**
https://bitbucket.org/livegamesio/client-api-dotnet

----------

> **Not:**
> <i class="icon-pencil"></i> at API queries HTTP Authorization Header: "Bearer _JWT.TOKEN.STRING_" usage standards are applied.

> <i class="icon-pencil"></i> to speed-up your integration process or to understand structure of the LG API you can use below libraries which developed by LG. With this libraries you can generate JWT tokens and create Seamless Callback methods.
>
**LG Libraries:**

>  * [PHP](https://bitbucket.org/livegamesio/client-api-php)
>  * [Node.js](https://bitbucket.org/livegamesio/client-api-nodejs)
>  * [.NET](https://bitbucket.org/livegamesio/client-api-dotnet)


----------


Wallet Integrations
-----

> **Notice:**
> To proceed above flow , API-BUS settings should be configured properly from the BackOffice panel.


**API Security Methods**

LG recommends to use one of below methods to check data security and trust relation between client - API server.

 1. **JWT**
	 Requests are send with encrypted JWT format with using apiSecret key.     Reponses are accept only with same method.

 2. **SecurityKey**
Requests are send with JSON data and security-key with generated with this JSON data.  


`TODO: ApiBus Örneği konulmalı mı`  



###**SeamlessWallet Integration**

For real-time queries to LG-API for user transactions & use One-Wallet this integration method should be selected by using below methods.


To establish connection between LG-API and Client Plaform you should provide an open SEAMLESS_URL to LiveGames. LG will post all requests to this URL.
Example: http://www.yoursite.com/livegamesapi

> **Not:**
>  For Seamless settings, in BackOffice  under the **Api Yönetimi** section you need to enter API details and click **"ApiBus Ayarları"** in SEAMLESS_URL section you need the provide URL or we can do this settings for you.


####**POST DATA (FormData)**

LG-API server will POST data as FORM format which includes  `lgAction`, `lgData` and if you select security type as "securityHash"  `lgSec` keys parameters.

Below you can find sent parameters descriptions;

#####**lgAction**

lgAction key using to specify action to do. There is two service in lgActioin key to get user balance details and transactions.

 * `GetWallet`
	_to get user balance(wallet) details._
 * `UpdateWallet`
	_to post user balance transactions._ (card buy, reward etc.)


#####**lgSec**

lgSec is key which includes security code for SecurityHash method. in  lgData all keys after sort by alphabetic,  this key depends of position on other keys  `.`  added and end of value with adding apiSecret , with md5  hash generating.

in LiveGames\ClientApiPHP library usage method like below.
```php
public function makeSecurityKey($data = []){
	$data = (array)$data;
	ksort($data); //sort by keys
	$dataCollection = [];
	foreach ($data as $key => $val) {
		if (!is_array($val) && !is_bool($val) && !is_object($val)){
			array_push($dataCollection,$val);
		}
	}
	array_push($dataCollection,$this->apiSecret); //end of value apisecret should be added
	return md5(implode(".", $dataCollection));
}
```


#####**lgData**

This key (lgData) includes values which post by LG-API server. This values should be confirm by one of security method which describe in above security methods (JWT or SecureHash)

in lgData, which POST to your handler,  JSON datas changes depends of action & type with below format.

> Requests via JWT additionally from below requests this key values will be added while sending.
>   `apiKey` , `game`,  `exp`,  `nbf`,  `iat`,  `jti`


`GetWallet`
```js
{
	"uid":"demo1", //USERID
	"ts":1484035774 //TIMESTAMP
	...
}
```
------


`Buy -> Card`
```js
{
    "game" : "tombala",
	"id":"5a3fc9ea-d04f-4d77-a43d",
	"uid":"demo1", //USERID
	"sid":295159, //SESSIONID
	"action":"buy",
	"type":"card",
	"amount":50,
	"cid":"7", //CARDID
	"ts":1484035774 //TIMESTAMP
}
```
------

`Buy -> RandomCard`
```js
{
    "game" : "tombala",
	"id":"5a3fc9ea-d04f-4d77-a43d",
	"uid":"demo1",
	"sid":295159,
	"action":"buy",
	"type":"randomcards",
	"amount":50,
	"cid":"7,130,80",
	"ts":1484035774
}
```
------

`Reward -> Card`
```js
{
	"id": "868bb8a9-6d3e-4544-a137-6f4448c3a37a", //LiveGames TransactionID
	"uid": "demo1", //UserID
	"sid": 295152, //SessionID
	"action": "reward",
	"type": "card",
	"amount": 50,
	"cid": "29", //CardID
	"cw": 2, //Prize Shareholders
	"rewards": "line1", // Reward types
	"ts": 1484035727 //timestamp
}
```

>  for more than 1 reward to 1 card rewards will be send like below format   `"rewards": "line1,line2,line3,line4"`

**Reward type formats;**

`line1 - 1. Cinko
line2 - 2. Cinko
line3 - Tombala
line4 - Tulum`

**Extra Reward type formats;**

`xr-5-1-15  (First 15 Ball)
xr-5-1-5 (First 5 Ball)`


------

`Reward-Jackpot`
```js
{
    "game" : "tombala",
	"id":"570a7fed-3eff-4f0b-b1a9-02b5e19a8fb2",
	"uid":"demo1",
	"sid":0,
	"action":"reward",
	"type":"jackpot",
	"amount":1.52,
	"ts":1479078182
}
```

------

`Reward-TossUp`
```js
{
    "game" : "tombala",
	"id":"0cdfc896-48b7-4320-a023",
	"uid":"demo1",
	"sid":208779,
	"action":"reward",
	"type":"tossup",
	"amount":150,
	"ts":1478469827
}
```

------

`Penalty-TossUp`
```js
{
    "game" : "tombala",
	"id":"b0cbe731-0d3c-4736-baa4",
	"uid":"demo1",
	"sid":212751,
	"action":
	"penalty",
	"type":"tossup",
	"amount":26,
	"ts":1478736815
}
```
------


**Rollback Methods**
`Rollback-Refund`
```js
{
    "game" : "tombala",
	"id" : "54c36ccf-3c79-4aa5-87f4-8a427bdfc495",
    "tid" : "b0cbe731-0d3c-4736-baa4-8a427bdfc495", //related transactionId
	"uid" : "demo1",
	"sid" : 412751,

	"action" : "rollback",
    "method" : "refund",
	"type" : "card",
    "cid" : "110",

	"amount" : 3,
	"ts" : 1478736815
}
```

------


`Rollback-Void-Reward`
```js
{
    "game" : "tombala",
	"id" : "54c36ccf-3c79-4aa5-87f4-8a427bdfc495",
    "tid" : "b0cbe731-0d3c-4736-baa4-8a427bdfc495",
	"uid" : "demo1",
	"sid" : 412751,

	"action" : "rollback",
    "method" : "void",
	"type" : "reward",
    "cid" : "110",
    "rewards" : "line2",

    "reason" : 1002,

	"amount" : 80,
	"ts" : 1478736815
}
```
`Rollback-Void-Jackpot`
```js
{
    "game" : "tombala",
	"id" : "54c36ccf-3c79-4aa5-87f4-8a427bdfc495",
    "tid" : "b0cbe731-0d3c-4736-baa4-8a427bdfc495",
	"uid" : "demo1",
	"sid" : 412751,

	"action" : "rollback",
    "method" : "void",
	"type" : "jackpot",

    "reason" : 1002,

	"amount" : 80,
	"ts" : 1478736815
}
```

`Rollback-Void-TossUp`
```js
{
    "game" : "tombala",
	"id" : "54c36ccf-3c79-4aa5-87f4-8a427bdfc495",
    "tid" : "b0cbe731-0d3c-4736-baa4-8a427bdfc495",
	"uid" : "demo1",
	"sid" : 412751,

	"action" : "rollback",
    "method" : "void",
	"type" : "tossup",

    "reason" : 1002,

	"amount" : 80,
	"ts" : 1478736815
}
```

**Rollback Reason List**
  - 1000: SYSTEM_AUTO_ROLLBACK
  - 1001: SYSTEM_MANUEL_ROLLBACK
  - 1002: CLIENT_MANUEL_ROLLBACK
  - 2001: SESSION_CANCELLED
  - (@todo: update list)


###**Seamless Response Create (Response)**

If you send requests via JWT to API server. All the responses should be in JWT format. Same for SecureHash method.


#### GetWallet Response

User current balance should send in  `credit` key. in in case of no value in `credit`  key response balance will be return as "0" (zero)

You can see GetWallet responses depends of Security methods.

##### JWT:

```json
{
"response" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXlsb2FkIjp7ImNyZWRpdCI6MTAwfSwiYXBpS2V5IjoiTGl2ZUdhbWVzQXBpS2V5IiwiZ2FtZSI6InRvbWJhbGEiLCJpYXQiOjE0NzAxNTU4NjgsImV4cCI6MTQ3MDI0MjI2OCwianRpIjoiZjcwMzEyYXgifQ.OI5TIyLYkumHt5Va-igRWSsVMkX1eG5jmm7cnKfSXWU"
}
```

#####SecurityKey (Hashed):

```json
{
   response:{
      "payload" : {
         "credit" : 100
      }   
   }
}
```


#### UpdateWallet Response

LG-API server is POST an uniqe `id`  for each transaction to your callback URL.  This key is define the transaction id at LG-API side.

In response from your side there should be an response id.  If there is no any response id from your answer to LG-API.


- If the action is "buy->card"  an error message will be send to the player and action will be canceled.

- If the action is "reward" or "penalty" LG-API server is trying to send POST data maximum 100 times.

**Response Sample** *(SecurityHash)*
```json
{
   response:{
      "payload" : {
	     "id" : "abcdefgh123",
         "credit" : 100
      }   
   }
}
```

> Response id not required to be unique.



###**MultiWallet Entegrasyonu**


`TODO: Dökümantasyon`




.
.
.
.
.
.
.
.
.

----------

### İçerik ve Başlıklar

[TOC]

----------

**i can fly!**

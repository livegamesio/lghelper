
###Seamless Response

####GetWallet Response
```json
{
    "response" :  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXlsb2FkIjp7ImNyZWRpdCI6MTAwfSwiaWF0IjoxNDcwMTU1ODY4LCJleHAiOjE0NzAyNDIyNjgsImp0aSI6ImY3MDMxMmF4In0.nJFkm_AIFL1LIroe5nHF29WQ2Ulms1XXNaevm3F8Nxo"
}
```

JWT Data
```js
{
  "payload": {
    "credit": 100
  },
  "iat": 1470155868,
  "exp": 1470242268,
  "jti": "f70312ax"
}
```

------

####UpdateWallet (success)

```json
{"response":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0ODM4MTAyNDMsImV4cCI6MTQ4Mzg5NjY0MywicGF5bG9hZCI6eyJpZCI6IkNMSUVOVF9UUkFOU0FDVElPTl9JRCIsInVpZCI6IlVzZXIxIiwiY3JlZGl0IjoxMjMuMTJ9LCJlcnJvcnMiOltdfQ.pjc5L2rMtM8GP0cY3lrcYcJjuNJBdryKsF09ywFFN1k"}
```

> if `"id": "CLIENT_TRANSACTION_ID"`  return null,false etc. transation is not accepted by side. If you return response id (alphanumeric) this transaction means by accepted you.


JWT Data
```js
{
  "iat": 1483810243,
  "exp": 1483896643,
  "payload": {
    "id": "CLIENT_TRANSACTION_ID",
    "uid": "User1",
    "credit": 123.12
  },
  "errors": []
}
```

------

####UpdateWallet (fail)
```json
{"response":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0ODM4MTAyNDMsImV4cCI6MTQ4Mzg5NjY0MywicGF5bG9hZCI6bnVsbCwiZXJyb3JzIjpbIkJBTEFOQ0VfTk9UX0VOT1VHSCJdfQ.BV2SBhRFHnidLqZsnIewIyhA4GCZRdfCvLTjZsU9zYI"}
```

JWT Data
```js
{
  "iat": 1483810243,
  "exp": 1483896643,
  "payload": null,
  "errors": ["BALANCE_NOT_ENOUGH"]
}
```
